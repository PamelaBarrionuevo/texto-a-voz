"""
    Convertir texto a voz (TTS) con
    Python usando gTTS
    Ejemplo 5: TTS con velocidad lenta
    @author parzibyte
"""
from gtts import gTTS
tts = gTTS('Hola mundo. Estamos convirtiendo texto a voz con Python.', lang='es-us', slow=True)
tts.save("5_hola_mundo.mp3")